package com.scripps.qa;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

/**
 * Created by 161803 on 9/13/2015.
 */
public class gmailTest {
    String username = "qascripps@gmail.com";
    String password = "Sai123456";
    private static WebDriver driver;
   // DesiredCapabilities caps;
   // @BeforeClass
//    public void Startup(){
//
//        driver = new FirefoxDriver();
//
//    }
   @Parameters({"platform", "browser", "version", "url"})
   @BeforeTest(alwaysRun = true)
    public void launchApplication(String platform, String browser, String version, String url) throws MalformedURLException {

       DesiredCapabilities caps = new DesiredCapabilities();
        //Platforms
        if (platform.equalsIgnoreCase("Windows"))
            caps.setPlatform(org.openqa.selenium.Platform.WINDOWS);

        if (platform.equalsIgnoreCase("MAC"))
            caps.setPlatform(org.openqa.selenium.Platform.MAC);
        if (platform.equalsIgnoreCase("Andorid"))
            caps.setPlatform(org.openqa.selenium.Platform.ANDROID);
        //Browsers
        if (browser.equalsIgnoreCase("Internet Explorer")) {
            File file = new File("C:\\SelGrid\\IEDriverServer.exe");
            System.setProperty("webdriver.ie.driver",file.getAbsolutePath()+"/IEDriverServer.exe");
            caps = DesiredCapabilities.internetExplorer();   // internetExplorer   internetExplorer
            caps.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
            // driver = new InternetExplorerDriver(caps);
        }
        if (browser.equalsIgnoreCase("chrome")) {
            File file = new File("C:\\SelGrid\\chromedriver.exe");
            System.setProperty("webdriver.chrome.driver", file.getAbsolutePath()+"/chromedriver.exe");
            //  System.setProperty("webdriver.chrome.driver",file.getAbsolutePath()+"/chromedriver.exe");
            caps.setPlatform(org.openqa.selenium.Platform.ANY);;
            caps = DesiredCapabilities.chrome();   // chrome
            // caps.setVersion("44.0.2403.157 m");
            //caps.setPlatform(Platform.extractFromSysProperty("WINDOWS"));

            // driver = new InternetExplorerDriver(caps);
        }

        if (browser.equalsIgnoreCase("Firefox")){
            caps = DesiredCapabilities.firefox();
            //caps.setPlatform(org.openqa.selenium.Platform.ANY);;
        }


        if (browser.equalsIgnoreCase("iPad")) {
            caps = DesiredCapabilities.ipad();
        }
        if (browser.equalsIgnoreCase("Android")) {
            caps = DesiredCapabilities.android();
        }
        //Version
       //driver = new FirefoxDriver();
    //   Selenium selenium = new DefaultSelenium("localhost", 4444, "*firefox", "http://jenkins-ci.org/");
         driver = new RemoteWebDriver(new URL("http://SNKXW-SMANIKON2:4444/wd/hub"), caps);
      //  driver = new RemoteWebDriver(new URL("http://10.69.5.134:4444/wd/hub"), caps);
    /*    caps.setVersion(version);
        if (browser.equalsIgnoreCase("Internet Explorer")) {
            driver = new RemoteWebDriver(new URL("http://10. 69.130.16:5556/wd/hub"), caps);
        }
        if (browser.equalsIgnoreCase("Firefox")){
            driver = new RemoteWebDriver(new URL("http://10.69.130.16:5555/wd/hub"), caps);
        }

        if (browser.equalsIgnoreCase("chrome")){
            driver = new RemoteWebDriver(new URL("http://10.69.130.16:5554/wd/hub"), caps);
        }
*/
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        // Open the BMI Calculator Application
        driver.get(url);
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);

    }

    @Test(description="Google Login2")
    public void GoogleLoginTestTwo() throws Exception{
        driver.get("http://www.gmail.com");
        driver.manage().window().maximize();
        //System.out.println("gmail");
        captureScreenShot(driver);
        assertEquals("Sign in", driver.findElement(By.id("signIn")).getAttribute("value"));
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.id("Email")).sendKeys(username);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.id("next")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//input[@id='Passwd']")).sendKeys(password);
        //  driver.findElement(By.id("Passwd")).sendKeys(password);
        driver.findElement(By.id("signIn")).click();
        Thread.sleep(10000);
        driver.switchTo().defaultContent();
        captureScreenShot(driver);

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.MINUTES);
        //       driver.findElement(By.xpath("//span[@class='gb_k']")).click();
        driver.findElement(By.xpath("//a[@href='https://accounts.google.com/SignOutOptions?hl=en&continue=https://mail.google.com/mail&service=mail']")).click();

        driver.manage().timeouts().implicitlyWait(40, TimeUnit.MINUTES);
        captureScreenShot(driver);
        driver.switchTo().activeElement();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.MINUTES);
        driver.findElement(By.xpath("//a[contains(.,'Sign out')]")).click();
//        //   driver.findElement(By.xpath("//a[@class='gb_hc gb_oc gb_a']")).click();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.MINUTES);

    }
//    @Test(description="Google Login3")
//    public void GoogleLoginTestThree() throws Exception{
//        driver.get("http://www.gmail.com");
//        driver.manage().window().maximize();
//        //System.out.println("gmail");
//        captureScreenShot(driver);
//        assertEquals("Sign in", driver.findElement(By.id("signIn")).getAttribute("value"));
////        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
////        driver.findElement(By.id("Email")).sendKeys(username);
////        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
////        driver.findElement(By.id("next")).click();
////        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
////        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
////        driver.findElement(By.xpath("//input[@id='Passwd']")).sendKeys(password);
////        //  driver.findElement(By.id("Passwd")).sendKeys(password);
////        driver.findElement(By.id("signIn")).click();
////        Thread.sleep(10000);
////        driver.switchTo().defaultContent();
////        captureScreenShot(driver);
////
////        driver.manage().timeouts().implicitlyWait(30, TimeUnit.MINUTES);
////        //       driver.findElement(By.xpath("//span[@class='gb_k']")).click();
////        driver.findElement(By.xpath("//a[@href='https://accounts.google.com/SignOutOptions?hl=en&continue=https://mail.google.com/mail&service=mail']")).click();
////
////        driver.manage().timeouts().implicitlyWait(40, TimeUnit.MINUTES);
////        captureScreenShot(driver);
////        driver.switchTo().activeElement();
////        driver.manage().timeouts().implicitlyWait(20, TimeUnit.MINUTES);
////        driver.findElement(By.xpath("//a[contains(.,'Sign out')]")).click();
////        //   driver.findElement(By.xpath("//a[@class='gb_hc gb_oc gb_a']")).click();
////        driver.manage().timeouts().implicitlyWait(30, TimeUnit.MINUTES);
//
//    }
    @AfterClass
    public void teardown(){
        driver.quit();
    }
    public static void captureScreenShot(WebDriver driver){
// Take screenshot and store as a file format
        File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        try {
         // now copy the  screenshot to desired location using copyFile method
            FileUtils.copyFile(src, new File("C:/logs/" + System.currentTimeMillis() + ".png"));
        } catch (IOException e)

         {
            System.out.println(e.getMessage());
        }
    }

}
